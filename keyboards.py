from vk_api.keyboard import VkKeyboard, VkKeyboardColor
def menu_board(id_):

    labels_menu = ['Дискретка\n', 'СРМА\n', 'Элтех\n', 'Решатель булевых функций\n', 'Вопрос к старосте\n',
                       'расписание']
    menu_buttons = VkKeyboard(one_time=True)
    for label in labels_menu:
        if label[len(label) - 1:] == '\n':
                menu_buttons.add_button(label=label[:-1], color=VkKeyboardColor.PRIMARY)
                menu_buttons.add_line()
        else:
                menu_buttons.add_button(label=label, color=VkKeyboardColor.PRIMARY)

    return menu_buttons.get_keyboard()

def elteh_keyboard(id):
    labels_elteh = ['Elteh Google drive link', 'Back']
    elteh_buttons = VkKeyboard(one_time=True)
    for label in labels_elteh:
        elteh_buttons.add_button(label=label, color=VkKeyboardColor.PRIMARY)
    return  elteh_buttons.get_keyboard()
def diskret_keyboard(id):
    labels_elteh = ['Diskret Google drive link', 'Back']
    elteh_buttons = VkKeyboard(one_time=True)
    for label in labels_elteh:
        elteh_buttons.add_button(label=label, color=VkKeyboardColor.PRIMARY)
    return  elteh_buttons.get_keyboard()
def week_keyboard(id):
    labels_elteh = ['Числитель', 'Знаменатель']
    elteh_buttons = VkKeyboard(one_time=True)
    for label in labels_elteh:
        elteh_buttons.add_button(label=label, color=VkKeyboardColor.PRIMARY)
    return elteh_buttons.get_keyboard()
def day_keyboard(id):
    # , 'Вт', "Ср", "Чт", "Пт"
    menu_buttons_ = ['Пн\n', 'Вт\n', 'Ср\n', 'Чт', 'Пт']
    menu_buttons = VkKeyboard(one_time=True)
    for label in menu_buttons_:
        if label[len(label) - 1:] == '\n':
            menu_buttons.add_button(label=label[:-1], color=VkKeyboardColor.PRIMARY)
            menu_buttons.add_line()
        else:
            menu_buttons.add_button(label=label, color=VkKeyboardColor.PRIMARY)

    return menu_buttons.get_keyboard()





