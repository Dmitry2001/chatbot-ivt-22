import keyboards
import handlers
from keyboards import menu_board, elteh_keyboard
INTENTS = [
    {
        'name' : 'Меню',
        'tokens' : ['меню', 'Меню', 'М'],
        'scenario' : 'menu',
        'answer' : None,
        'checker' : 'auth_check'
    },
{
        'name' : 'Элтех',
        'tokens' : ['элтех', 'Элтех'],
        'scenario' : 'elteh',
        'answer' : None,
        'checker' : 'auth_check'
    },
{
        'name' : 'Back',
        'tokens' : ['Back'],
        'scenario' : 'menu',
        'answer' : None,
        'checker' : 'auth_check'
    },
{
        'name' : 'LinkElteh',
        'tokens' : ['Elteh Google drive link'],
        'scenario' : None,
        'answer' : 'https://drive.google.com/drive/folders/1XntcudGqq5iUquCgEDkwGgZIm6R0nFo8?usp=sharing',
        'checker' : 'auth_check'
    },
{
        'name' : 'Registration',
        'tokens' : ['регистрация'],
        'scenario' : 'registration',
        'answer' : None,
        'checker' : 'auth_check'
    },

{
        'name' : 'Дискретка',
        'tokens' : ['Дискретка'],
        'scenario' : 'diskret',
        'answer' : None,
        'checker' : 'auth_check'
    },
{
        'name' : 'Дискретка',
        'tokens' : ['Решатель булевых функций'],
        'scenario' : 'BuleForm',
        'answer' : None,
        'checker' : 'auth_check'
    },
{
        'name' : 'Полином',
        'tokens' : ['Полином Жегалкина'],
        'scenario' : 'Polinom',
        'answer' : None,
        'checker' : 'auth_check'
    },
{
        'name' : 'Вопросы',
        'tokens' : ['Вопрос к старосте', 'Вопрос'],
        'scenario' : 'Questions',
        'answer' : None,
        'checker' : 'auth_check'
    },
{
        'name' : 'Shedule',
        'tokens' : ['расписание'],
        'scenario' : 'shedule',
        'answer' : None,
        'checker' : 'auth_check'
    },
{
        'name' : 'Shedule',
        'tokens' : ['рассылка'],
        'scenario' : 'mailing',
        'answer' : None,
        'checker' : 'auth_check'
    }

    ]
SCENARIOS = {
    'menu' :
        {
            'first_step' : 'step1',
            'steps' :
                {
                    'step1' :
                        {
                            'text' : 'Здравствуйте, уважаемый пользователь',
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : 'elteh_handler',
                            'keyboard' : menu_board,
                            'next_step' : None
                        }
                }
        },
    'elteh' :
        {
            'first_step' : 'step1',
            'steps' :
                {
                    'step1' :
                        {
                            'text' : 'Здравствуйте, уважаемый пользователь',
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : 'elteh_handler',
                            'keyboard' : elteh_keyboard,
                            'next_step' : None
                        }
                }
        },
'registration' :
        {
            'first_step' : 'step1',
            'steps' :
                {
                    'step1' :
                        {
                            'text' : 'Здравствуйте, уважаемый пользователь, введите своё имя  фамилию через пробел',
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : 'ns_handler',
                            'keyboard' : None,
                            'next_step' : 'step2'
                        },
'step2' :
                        {
                            'text' : 'Insert gmail',
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : 'gmail_handler',
                            'keyboard' : None,
                            'next_step' : 'step3'
                        },
'step3' :
                        {
                            'text' : 'Cпасибо за регистрацию! Теперь материалы вам доступны',
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : None,
                            'keyboard' : None,
                            'next_step' : None
                        }
                }
        },
'diskret' :
        {
            'first_step' : 'step1',
            'steps' :
                {
                    'step1' :
                        {
                            'text' : 'Здравствуйте, уважаемый пользователь',
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : 'diskret',
                            'keyboard' : keyboards.diskret_keyboard,
                            'next_step' : None
                        }
                }
        },
'Polinom' :
{
            'first_step' : 'step1',
            'steps' :
                {
                    'step1' :
                        {
                            'failure_text': "Попробуйте ещё раз!",
                            'handler': None,
                            'keyboard': None,
                            'next_step': None,
                            'text' : handlers.polinom_handler,
                        },
                }
        },
'BuleForm' :
{
            'first_step' : 'step1',
            'steps' :
                {
                    'step1' :
                        {
                            'text' : 'Введите булев вектор функции',
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : 'bule_handler',
                            'keyboard' : None,
                            'next_step' : 'step2'
                        },
'step2' :
                        {
                            'text' : handlers.bule_form_choice,
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : 'bule_form_choice',
                            'keyboard' : None,
                            'next_step' : None
                        }
                }
        },
'Questions' :
{
            'first_step' : 'step1',
            'steps' :
                {
                    'step1' :
                        {
                            'text' : 'Введите ваш вопрос',
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : 'question_handler',
                            'keyboard' : None,
                            'next_step' : 'step2'
                        },
'step2' :
                        {
                            'text' : 'Ваш вопрос успешно зафиксирован',
                            'failure_text' : "Попробуйте ещё раз!",
                            'handler' : None,
                            'keyboard' : None,
                            'next_step' : None
                        }
                }
        },
'shedule':
    {
'first_step' : 'step1',
            'steps' :
                {
'step1': {
        'text': 'Числитель или знаменатель?',
        'failure_text': "Попробуйте ещё раз!",
        'handler': 'shedule_handler_week',
        'keyboard': keyboards.week_keyboard,
        'next_step': 'step2'
},
'step2' : {
'text': 'Выберите день',
        'failure_text': "Попробуйте ещё раз!",
        'handler': 'shedule_day_handler',
        'keyboard': keyboards.day_keyboard,
        'next_step': 'step3'
},
                    'step3' : {
'text': handlers.shedule_day_handler,
        'failure_text': "Попробуйте ещё раз!",
        'handler': None,
        'keyboard': None,
        'next_step': None

                    }
}
}
}
DEFAULT_ANSWERS = 'Здравствуйте, мне не понятен ваш запрос!'