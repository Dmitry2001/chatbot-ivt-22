import itertools
import math
import termcolor
class BuleForms():
    def __init__(self, bule_vector):
        self.bule_vector = bule_vector
        if len(bule_vector) & (len(bule_vector) - 1)  == 0:
            self.n = int(math.log2(len(bule_vector)))
        else:
            raise ValueError('Не является степенью двойки')
        self.polinom_jegalkin = None
    def reformat_bule_vector(self):
        bule_vector = []
        for i in self.bule_vector:
            bule_vector.append(int(i))
        self.bule_vector = bule_vector
    def jegalkin_form(self):
        self.reformat_bule_vector()
        triangle_matrix = [self.bule_vector]
        for j in range(len(self.bule_vector) - 1):
            new_vector = []
            for i in range(len(triangle_matrix[j]) - 1):
                new_vector.append((triangle_matrix[j][i] + triangle_matrix[j][i + 1]) % 2)
            triangle_matrix.append(new_vector)
        polinom_array = []
        for i, element in enumerate(itertools.product([0,1], repeat = self.n)):
            if i == 0 and triangle_matrix[i][0] != 0:
                polinom_array.append('1')
            elif i != 0 and triangle_matrix[i][0] != 0:
                polinom_array.append('')
                for j in range(self.n):
                    if element[j] == 1:
                        polinom_array[len(polinom_array) - 1] += f'X{j + 1}'
                j = 0
            elif triangle_matrix[i][0] == 0:
                continue
        print(f'Полином Жегалкина для функции {self.bule_vector}', ' \u2295 '.join(polinom_array))
        with open('bule_function_analyze.txt', 'a',encoding='utf8') as f:
            f.write(f'Полином Жегалкина для функции  ' +  ' \u2295 '.join(polinom_array))
        self.polinom_jegalkin = polinom_array
        return 'полином Жегалкина ' + ' \u2295 '.join(polinom_array)

    def sdnf(self):
        polinom_array = []
        self.reformat_bule_vector()
        for i, element in enumerate(itertools.product([0,1], repeat = self.n)):
            if self.bule_vector[i] == 1:
                polinom_array.append([])
                for j in range(self.n):
                    if element[j] != 0:
                        polinom_array[len(polinom_array) - 1].append(f'X{j + 1}')
                    else:
                        polinom_array[len(polinom_array) - 1].append(f'~X{j + 1}')
            else:
                continue
        for element in polinom_array:
            polinom_array[polinom_array.index(element)] = '\u2227'.join(element)
        print(f'СДНФ для функции {self.bule_vector} ',
              ' \u2228 '.join(polinom_array))
        with open('bule_function_analyze.txt', 'a',encoding='utf8') as f:
            f.write(f'\n СДНФ для функции  '+
              ' \u2228 '.join(polinom_array) + '\n')
        return ' \u2228 '.join(polinom_array)
    def sknf(self):
        polinom_array = []
        self.reformat_bule_vector()
        for i, element in enumerate(itertools.product([0, 1], repeat=self.n)):
            if self.bule_vector[i] == 0:
                polinom_array.append([])
                for j in range(self.n):
                    if element[j] == 0:
                        polinom_array[len(polinom_array) - 1].append(f'X{j + 1}')
                    else:
                        polinom_array[len(polinom_array) - 1].append(f'~X{j + 1}')
            else:
                continue
        for element in polinom_array:
            polinom_array[polinom_array.index(element)] = '('+'\u2228'.join(element)+')'
        print( f'СКНФ для функции {self.bule_vector} ',' \u2227 '.join(polinom_array))
        self.sdnf = polinom_array
        return ' \u2227 '.join(polinom_array)
    def post_checking(self):
        class_post = {}
        if self.bule_vector[0] == 0:
            class_post['To'] = '+'
        else:
            class_post['To'] = '-'
        if self.bule_vector[len(self.bule_vector) - 1] == 1:
            class_post['T1'] = '+'
        else:
            class_post['T1'] = '-'
        for i in range(len(self.bule_vector)//2):
            if self.bule_vector[i] != self.bule_vector[len(self.bule_vector) - i - 1]:
                pass
            else:
                class_post['S'] = '-'
                break
        else:
            class_post['S'] = '+'
        suits = list(itertools.product([0, 1], repeat=self.n))
        class_post['M'] = '+'
        for i in range(len(suits)):
            if class_post['M'] != '-':
                for j in range(len(suits)):
                    if i < j:
                        monotonic_suit = True
                        for k in range(self.n):
                            if suits[i][k] > suits[j][k]:
                                monotonic_suit = False
                        if monotonic_suit and self.bule_vector[i] > self.bule_vector[j]:
                            class_post['M'] = '-'
                            break
                        else:
                            class_post['M'] = '+'
                    else:
                        continue
        class_post['L'] = '+'
        for element in self.polinom_jegalkin:
            if len(element) > 2:
                class_post['L'] = '-'
                break
        formatted = ''
        for key, value in class_post.items():
            formatted += (key + ' : ' + value + '\n')
            termcolor.cprint(' '.join([key, value]), color='yellow')
        return formatted




# polinom = BuleForms('10110111001010100001101010100111101101110011010101001100111001011011011100101010000110101010011110110111001101010100110011100101')
