from random import randint
import logging
import scenario
import vk_api
import handlers
from vk_api.bot_longpoll import  VkBotLongPoll, VkBotEventType as EventType
import group_data
logger = logging.getLogger('bot')
stream_handler = logging.StreamHandler()
logger.addHandler(hdlr=stream_handler)
logger.setLevel(level=logging.DEBUG)
# stream_handler.setLevel(level=logging.INFO)
stream_handler.setFormatter(logging.Formatter('%(levelname)s  %(asctime)s  %(message)s'))
file_handler = logging.FileHandler(filename='bot.log', mode='w', encoding='utf8')
logger.addHandler(hdlr=file_handler)
file_handler.setLevel(level=logging.DEBUG)
file_handler.setFormatter(logging.Formatter('%(levelname)s  %(asctime)s  %(message)s'))
from handlers import candidates
class UserState():
    def __init__(self, scenario_name, current_step, context=None):
        self.scenario_name = scenario_name
        self.current_step = current_step
        self.context = context or {}
        self.auth_check = None
class Bot():
    common_base = []
    def __init__(self, group_name, token):
        self.group_id = group_name
        self.token = token
        self.vk = vk_api.VkApi(token=self.token)
        self.long_poller = VkBotLongPoll(vk=self.vk, group_id=self.group_id)
        self.api = self.vk.get_api()
        self.states = dict()
    def get_user_info(self, id):
        return self.api.users.get(user_ids=id,
                                  access_token=group_data.token)[0]
    def run(self):
        for event in self.long_poller.listen():
            try:
                self.on_event(event=event)
            except Exception as exc:
                logger.exception(msg=f'Ошибка обработки события!')
    def on_event(self, event):
        if event.type != EventType.MESSAGE_NEW:
            logger.info(msg=f'Мы не умеем обрабатывать сообщение типа {event.type}')
            return
        with open('bot_upgrade\\ids.txt', 'r') as f:
            if str(event.object.peer_id)+'\n' in f.readlines():
                with open('bot_users.txt', 'r') as file:
                    for line in file:
                        self.common_base.append(str(line))
                if str(event.object.peer_id)+'\n' not in self.common_base and event.object.peer_id not in candidates:
                    text_for_sending = self.start_scenario(event.object.peer_id, 'registration')
                    candidates[event.object.peer_id] = UserState(scenario_name="registration", current_step='step1', context=None)
                else:
                    user_id = event.object.peer_id
                    if user_id in self.states or user_id in candidates:
                        text_for_sending = self.continue_scenario(event.object.text, user_id)
                    else:
                        for intent in scenario.INTENTS:
                            if any(token == event.object.text for token in intent['tokens']):
                                if intent['answer']:
                                    text_for_sending = intent['answer']
                                    break
                                else:
                                    text_for_sending = self.start_scenario(user_id, intent['scenario'])
                                    break
                        else:
                            text_for_sending = scenario.DEFAULT_ANSWERS
            else:
                text_for_sending = 'Извините, но вы не являетесь участником группы ИВТ-22, поэтому функции бота для вас недоступны!'
        if len(text_for_sending) == 2 and isinstance(text_for_sending, list):
            self.send(event=event, message=text_for_sending[0], keyboard=text_for_sending[1])
        else:
            self.send(event=event, message=text_for_sending)
    def continue_scenario(self, text, user_id):
        try:
            state = self.states[user_id] #suggest state of user
        except:
            state = candidates[user_id]
        step = scenario.SCENARIOS[state.scenario_name]['steps'][state.current_step] # getting dict of step
        handler = getattr(handlers, step['handler'])
        if handler(text=text, context=state.context, id=user_id):
            next_step = scenario.SCENARIOS[state.scenario_name]['steps'][step['next_step']] # getting dict
            # of next_step
            if next_step == None:
                try:
                    text_for_sending = step['text'](text, state.context, user_id)
                    self.states.pop(user_id)
                except:
                    text_for_sending = step['text']
                    self.states.pop(user_id)
            else:
                try:
                    text_for_sending = [next_step['text'](text, state.context, user_id)]
                except:
                    if next_step['keyboard']:
                        text_for_sending = [next_step['text'], next_step['keyboard'](user_id)]
                    else:
                        text_for_sending = [next_step['text']]
                if next_step['next_step']:
                    state.current_step = step['next_step']
                elif text_for_sending[0] == False:
                    text_for_sending = [step['failure_text']]
                else:
                    self.states.pop(user_id)
        else:
            text_for_sending = [step['failure_text']]

        if len(text_for_sending) == 2 and text_for_sending[1]:
            return text_for_sending
        else:
            return text_for_sending[0]
    def start_scenario(self, user_id, scenario_name):
        scenario1 = scenario.SCENARIOS[scenario_name]
        first_step = scenario1['first_step']
        step = scenario1['steps'][first_step]
        text_for_sending = step['text']
        if step['next_step'] == None:
            pass
        else:
            self.states[user_id] = UserState(scenario_name=scenario_name, current_step=first_step)
        if step['keyboard']:
            return [text_for_sending, step['keyboard'](user_id)]
        else:
            return text_for_sending
    def send(self, message, event, keyboard=None):
        # users_data = self.api.users.get(user_ids=event.object.peer_id, access_token=self.token)
        # print(self.api.users.get(user_ids=event.object.peer_id, access_token=self.token))
        print(event.object.peer_id)
        self.api.messages.send(message=message,
                               random_id=randint(100000, 1000000), peer_id=event.object.peer_id, keyboard=keyboard)
bot = Bot(group_name=group_data.group_id, token=group_data.token)
bot.run()
