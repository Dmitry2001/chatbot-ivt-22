import re
from datetime import datetime
from shedule import shedule_days
import bot_upgrade.BuleForms as BF
import gspread
candidates = {}
name_re = r'^[\w\-\s]{3,40}$'
email_re = r"(^[a-zA-Z0-9_.+-]+@gmail\.[a-zA-Z0-9-.]+$)"
def share_to_student(gmail, message):
    from google.oauth2 import service_account
    from googleapiclient.http import MediaIoBaseDownload, MediaFileUpload
    from googleapiclient.discovery import build
    SCOPES = ['https://www.googleapis.com/auth/drive']
    SERVICE_ACCOUNT_FILE = 'cred4.json'
    credentials = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE, scopes=SCOPES)
    service = build('drive', 'v3', credentials=credentials)
    permission1 = {
    'type': 'user',
    'role': 'reader',
    'emailAddress': gmail,  # Please set your email of Google account.
    }
    service.permissions().create(fileId='1o_Rs8AdB7sOfmjr-70SUzdV0kU0iBfuW',
                             body=permission1,
                             sendNotificationEmail=True, emailMessage=message).execute()
def handler_name(text, context, id):
    matched = re.match(pattern=name_re, string=text)
    if matched:
        context['name'] = text
        return True
    else:
        return False
def handler_email(text, context, id):
    matched = re.findall(pattern=email_re, string=text)
    if len(matched) > 0:
        context['email'] = matched[0]
        return True
    else:
        return False
def elteh_handler(text, context, id):
    return True
def ns_handler(text, context, id):
    if len(text.split(' ')) == 2:
        text = text.split(' ')
        context['name'] = text[0]
        context['surname'] = text[1]
        return True
    else:
        return False
def gmail_handler(text, context, id):
    matched = re.findall(pattern=email_re, string=text)
    if len(matched) > 0:
        context['email'] = matched[0]
        apisheet = gspread.service_account(filename='bot_upgrade\\creds.json')
        sheet = apisheet.open_by_key(key='1qxUl0b4VKV1zpfsTWLo1lUQoe-oT0XyI_fzIKIsRKaU').sheet1
        context['email'] = text
        del candidates[id]
        with open('bot_users.txt', 'a') as file:
            file.write('\n' + str(id) + '\n')
        sheet.append_row([id, context['name'], context['surname'], context['email'], 'Registered'])
        share_to_student(text, 'Вам открыт доступ к файлам!')
        return True
    else:
        return False
def polinom_handler(text, context, id):
    if text:
        Polinom = BF.BuleForms(bule_vector=context['bule'])
        return str(Polinom.jegalkin_form())
    else:
       return False
def bule_handler(text, context, id):
    if text:
        context['bule'] = text
        return True
    else:
       return False
def bule_form_choice(text, context, id):
    try:
        if text:
            Bule = BF.BuleForms(context['bule'])
            context[text] = str(Bule.jegalkin_form()) + '\n СДНФ ' + str(Bule.sdnf()) + '\n СКНФ ' + str(Bule.sknf()) + '\n Анализ по классам Поста\n ' + str(Bule.post_checking())
            return context[text]
        else:
            return False
    except:
        return False
def question_handler(text, context, id):
    if text:
        apisheet = gspread.service_account(filename='bot_upgrade\\creds.json')
        sheet = apisheet.open_by_key(key='1qxUl0b4VKV1zpfsTWLo1lUQoe-oT0XyI_fzIKIsRKaU').sheet1
        sheet2 = apisheet.open_by_key(key='1dVFPQv9KtdohOYlpzndfuKZs_EgPyTtD-O41jjaN3EY').sheet1
        col, row = sheet.find(str(id)).col, sheet.find(str(id)).row
        sheet2.append_row([sheet.cell(col=col + 1, row=row).value, sheet.cell(col=col + 2, row=row).value, sheet.cell(col=col + 3, row=row).value, text])
        return True
    else:
        return False
def shedule_handler_week(text, context, id):
    if text in ('Числитель', 'Знаменатель' ):
        context[text] = True
        return True
    else:
        return False
def shedule_day_handler(text, context, id):
    if context.get('Числитель', False):
        text_ = shedule_days['Числитель'][text]
        return text_
    elif context.get('Знаменатель', False):
        return shedule_days['Знаменатель'][text]








